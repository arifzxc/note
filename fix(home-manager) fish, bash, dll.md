## FISH

```
vi .config/fish/config.fish
```
input before end (ending)
```
clear
```

<b>change theme</b>
```
fish_config theme choose Lava 
fish_config theme save -y
```


## BASHRC

```
vi .bashrc
```
input (ending)
```
clear
/home/arifzxc/.nix-profile/bin/fish
```
