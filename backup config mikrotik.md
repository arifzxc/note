
## BACKUP MIRKOTIK WITH SERVER LINUX

Access super user
```
sudo su
```
Generate key (enter 3x, by default)
```
ssh-keygen
```

Transfer file public key to mikrotik
```
scp /root/.ssh/id_rsa.pub <user_mikrotik>@<ip_mikrotik>:.
```

Add to ssh key mikrotik (open terminal mikrotik)
```
user ssh-keys import public-key-file=id_rsa.pub user=<user_mikrotik>
```

Create folder and file
```
mkdir backup-mikrotik
```
```
touch autobackup-mikrotik
chmod 770 autobackup-mikrotik
vi autobackup-mikrotik
```

Input
```
#!/bin/bash

userName=<username>
target=<ip_mikrotik>

#getDate
getDate=`date +%d-%m-%Y`

#backupRouter
ssh $userName@$target "system backup save name="<nama_file>""
ssh $userName@$target "export compact file="<nama_file>""

sleep 5

#sendBackupFileToLocal
scp $userName@$target:"/<nama_file>.backup" /root/backup-mikrotik ; cd /root/backup-mikrotik ; mv <nama_file>.backup "<nama_file>($getDate).backup"
scp $userName@$target:"/<nama_file>.rsc" /root/backup-mikrotik ; cd /root/backup-mikrotik ; mv <nama_file>.rsc "<nama_file>($getDate).rsc"


#deleteBackupFile
ssh $userName@$target "file remove <nama_file>.backup"
ssh $userName@$target "file remove <nama_file>.rsc"
```

Run script (optional, just testing)
```
./autobackup-mikrotik
```

Run auto backup with crontab
```
crontab -e
```
Input (schedule run every 7 day of week)
```
0 0 * * */7 /root/autobackup-mikrotik
```

done