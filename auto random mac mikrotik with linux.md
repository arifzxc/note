Script 
```
#!/bin/bash

#Mikrotik
userName=arifzxc
target=192.168.1.1
P="-P100"
p="-p 100"

#Number random
end=$(shuf -i 0-99 -n 1)
mid1=$(shuf -i 0-99 -n 1)
mid2=$(shuf -i 0-9 -n 1)
bend=$(shuf -i 0-9 -n 1)

#Mac List
mac="E0:1F:${mid1}:${mid2}F:F${bend}:${end}"

echo ${mac}

#random mac
ssh $p $userName@$target "interface wireless set mac-address=${mac} numbers=wlan1"
```