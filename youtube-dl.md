# Youtube Downloader Command Line (yt-dl) SLOW SPEED

<h2>Download file to install</h2>

```
sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
```
Add permission (read & execute)
```
sudo chmod a+rx /usr/local/bin/youtube-dl
```
Install python3 pip
```
sudo apt install python3-pip
```
Upgrade youtube-dl with pip
```
pip3 install --upgrade youtube-dl
```


<h2>Example :</h2>

```
youtube-dl [OPTIONS] [URL]
```

For chek list download (audio or vidio)
```
youtube-dl -F https://youtube.com/alksjdklasdj
```
Input number list to download 
```
youtube-dl -f [number] https://youtube.com/alksjdklasdj
youtube-dl -f 10 https://youtube.com/alksjdklasdj
```
Or (default)
```
youtube-dl -o namafile.mp4 https://youtube.com/alksjdklasdj
```
```
youtube-dl https://youtube.com/alksjdklasdj
```