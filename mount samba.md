<h2>Mount</h2>

```
mount -t cifs //192.168.0.1/backup /mnt/backup -o username=<name>,password=<password>
```

<h2>Mount other folder</h2>

```
mount --bind /mnt/backup/ /home/arifzxc/Documents/DataServer/
```