# Youtube Downloader Command Line (yt-dlp) RECOMMEND

<h2>Download file to install</h2>

```
sudo wget https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -O /usr/local/bin/yt-dlp
```
Add permission (read & execute)
```
sudo chmod a+rx /usr/local/bin/yt-dlp
```
To update yt-dlp:
```
sudo yt-dlp -U
```


Install python3 pip
```
sudo apt install python3-pip
```
Update yt-dlp with pip (recommed)
```
python3 -m pip install -U yt-dlp
```
(optional)
```
python3 -m pip install --no-deps -U yt-dlp
```

<h2>Other installation</h2>

```
sudo add-apt-repository ppa:yt-dlp/stable    # Add ppa repo to apt
sudo apt update                              # Update package list
sudo apt install yt-dlp                      # Install yt-dlp
```

<h2>Example to run download:</h2>

```
yt-dlp [OPTIONS] [URL]
```

For chek list download (audio or vidio)
```
yt-dlp -F https://youtube.com/alksjdklasdj
```
Input number list to download 
```
yt-dlp -f [number] https://youtube.com/alksjdklasdj

yt-dlp -f 22 https://youtube.com/alksjdklasdj
```
Or (default)
```
yt-dlp -o namafile.mp4 https://youtube.com/alksjdklasdj

yt-dlp https://youtube.com/alksjdklasdj
```