# DVWA
DVWA (Damn Vurnelabel Web App) dapat digunakan untuk belajar SQL Injection / SQLmap untuk melakukan serangan ke Web & Database-nya

<h3>Download extension</h3>

Ubuntu
```
sudo apt install apache2 php php-xmlrpc php-mysql php-xdebug php-gd php-cli php-curl \
mysql-client mysql-server libphp-adodb libgd-dev php-pear \
php-common libapache2-mod-php unzip php-fpm \
php-bz2 php-xml imagemagick php-zip php-mbstring -y 

/etc/init.d/apache2 restart
/etc/init.d/mysql restart
```

Debian
```
sudo apt install apache2 php php-xmlrpc php-mysql php-xdebug php-gd php-cli php-curl \
mariadb-client mariadb-server libphp-adodb libgd-dev php-pear \
php-common libapache2-mod-php unzip php-fpm \
php-bz2 php-xml imagemagick php-zip php-mbstring -y 

/etc/init.d/apache2 restart
/etc/init.d/mariadb restart
```

<h3>Download file dan kasih permission</h3>

```
cd /var/www/html
git clone https://github.com/digininja/DVWA

cd /var/www/html/DVWA/external/phpids/0.6/lib/IDS
chmod -Rf 777 tmp
chown -Rf nobody.nogroup tmp
chmod -Rf 777 /var/www/html/DVWA/hackable/uploads/

cd /var/www/html/DVWA/config
mv config.inc.php.dist config.inc.php
```


<h3>Edit file php.ini, sesuaikan dengan versi php masing-masing.</h3>

```
vi /etc/php/7.4/cli/php.ini /etc/php/7.4/apache2/php.ini
```

Ubah 
```
allow_url_include = Off
```
Menjadi
```
allow_url_include = On
```

<h3>Edit file config.inc.php</h3>

```
vi /var/www/html/DVWA/config/config.inc.php
```

Ubah
```
$_DVWA[ 'recaptcha_public_key' ]  = ' ';
$_DVWA[ 'recaptcha_private_key' ] = ' ';
```
Menjadi
```
$_DVWA[ 'recaptcha_public_key' ]  = '6LdK7xITAAzzAAJQTfL7fu6I-0aPl8KHHieAT_yJg';
$_DVWA[ 'recaptcha_private_key' ] = '6LdK7xITAzzAAL_uw9YXVUOPoIHPZLfw2K1n5NVQ';
```

Ubah
```
$_DVWA[ 'default_security_level' ] = 'impossible';
```
Menjadi
```
$_DVWA[ 'default_security_level' ] = 'low';
```


<h3>Bikin database</h3>

```
mysql -u root
```
```
create user <nama_user>;
grant all on <nama_database>.* to <nama_user>@localhost identified by '<password>';
flush privileges;
grant all on <nama_database>.* to '<nama_user>'@'%';
flush privileges;
exit;
```


<h3>Edit configurasi database, sesuaikan dengan database yang dibuat.</h3>

```
vi /var/www/html/DVWA/config/config.inc.php
```
```
$_DVWA = array();
$_DVWA[ 'db_server' ] = 'localhost';
$_DVWA[ 'db_database' ] = '<nama_database>';
$_DVWA[ 'db_user' ] = '<user_database>';
$_DVWA[ 'db_password' ] = '<password>';
```

<h3>Restart service</h3>

```
/etc/init.d/apache2 restart
/etc/init.d/mariadb restart
/etc/init.d/mysql restart
```

<h3>Akses web DVWA</h3>


- http://ip-server/DVWA/
- http://192.168.0.80/DVWA/
- http://192.168.0.100/DVWA/
- http://192.168.0.111/DVWA/

Klik
```
Create / Reset Database
```
next klik login
```
username:  admin
password: password
```
