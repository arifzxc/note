Create file rc.local in /etc/rc.local

```
printf '%s\n' '#!/bin/bash' 'exit 0' | sudo tee -a /etc/rc.local

```
or
```
sudo touch /etc/rc.local
echo '#!/bin/bash' > /etc/rc.local
echo '' >> /etc/rc.local
echo 'exit 0' >> /etc/rc.local

```

```
sudo chmod +x /etc/rc.local

```

create systemd
```
sudo vi /etc/systemd/system/rc-local.service

```

input
```
[Unit]
Description=/etc/rc.local Compatibility
ConditionPathExists=/etc/rc.local

[Service]
Type=forking
ExecStart=/etc/rc.local start
TimeoutSec=0
StandardOutput=tty
RemainAfterExit=yes
SysVStartPriority=99

[Install]
WantedBy=multi-user.target
```

running serverice rc-local
```
systemctl enable rc-local.service 
systemctl start rc-local.service 

```
