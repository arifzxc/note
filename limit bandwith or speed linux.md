Enable the limitation
```
iptables -I FORWARD -m iprange --dst-range 192.168.1.50-192.168.1.70 -j DROP
iptables -I FORWARD -m iprange --dst-range 192.168.1.50-192.168.1.70 -m limit --limit 100/sec -m state --state ESTABLISHED -j ACCEPT
```

Disable it
```
iptables -D FORWARD 1
iptables -D FORWARD 1
```