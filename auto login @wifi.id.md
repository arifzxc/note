# Password Only

add system > script, name "@wifi.id"
```
:local username "password.jkl1@wms.idxxxx" #use dot, type any alfabet and number
:local password "passwordxxxx"
:local gwid "GW-xxxx-xxxx"
:local wlanid "ID-xxxxxx";
:local ip [:pick [:ip address get [:ip addres find interface=wlan1] address] 0 ([:len [:ip address get [:ip addres find interface=wlan1] address]]-3) ];
:local mac [/interface wireless get [ find default-name=wlan1] mac-address];
:tool fetch mode=https http-header-field="Content-Type: application/x-www-form-urlencoded; charset=UTF-8,User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36,Referer: https://welcome2.wifi.id" http-method=post http-data="username=$username&password=$password" url="https://welcome2.wifi.id/authnew/login/check_login.php?ipc=$ip&gw_id=$gwid&mac=$mac&redirect=http://www.msftconnecttest.com/redirect&wlan=$wlanid&load_wp=";
```

add system > scheduler<br>
manual
```
:if ([:ping 8.8.8.8 count=5 interface=wlan1]=0) do={
	:system script run @wifi.id;
}
```

automatic
```
/system scheduler add name="ping test @wifi.id" start-time=startup interval=00:00:10 policy=ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon on-event=":if ([:ping 8.8.8.8 count=5 interface=wlan1]=0) do={ :system script run @wifi.id; }"
```
