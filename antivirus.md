# Instalasi antivirus clamav

atau kalau mau di jalan dengan MailScanner
```
apt install clamav clamav-daemon 
```
proses update database clamav akan dilakukan secara automatis. Jika kita ingin memaksa untuk melakukan update database, hal ini dapat dilakukan menggunakan perintah
```
freshclam
```
Pastikan komputer tersambung ke Internet, karena pusat database virus clamav terdapat di Internet.
<br>untuk menscan folder / file lakukan
```
clamscan -r --remove /folder/yang/akan/di/scan
```
dengan menggunakan switch --remove akan di delete semua file yang ber-virus. ClamAV memang tidak di persenjatai untuk memperbaiki file yang terinfeksi. Pilihan yang ada adalah mendelete file tersebut atau mengkarantina-nya. Lebih aman jika kita men-delete file yang ber-virus.

