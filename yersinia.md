# YERSINIA
<b> Yersinia </b> is a framework for performing layer 2 attacks. The following protocols have been implemented in Yersinia current version: Spanning Tree Protocol (STP), VLAN Trunking Protocol (VTP), Hot Standby Router Protocol (HSRP), Dynamic Trunking Protocol (DTP), IEEE 802.1Q, Cisco Discovery Protocol (CDP), Dynamic Host Configuration Protocol (DHCP) and, finally, the Inter-Switch Link Protocol (ISL).
Some of the attacks implemented will cause a DoS in a network, other will help to perform any other more advanced attack, or both. In addition, some of them will be first released to the public since there isn't any public implementation.

Yersinia will definitely help both pen-testers and network administrators in their daily tasks.

Some of the mentioned attacks are DoS attacks, so TAKE CARE about what you're doing because you can convert your network into an UNSTABLE one.

A lot of examples are given at this page EXAMPLES section, showing a real and useful program execution.

# Yersinia - A FrameWork for layer 2 attacks
# Synopsis
```
yersinia [-hVGIDd] [-l logfile] [-c conffile] protocol [-M] [protocol_options]
````

# Options

<b>-h</b>, --help <br> Help screen.

<b>-V</b>, --Version <br> Program version.

<b>-G</b> <br>Start a graphical GTK session.

<b>-I</b>, --interactive <br> Start an interactive ncurses session.
 
<b>-D</b>, --daemon <br> Start the network listener for remote admin (Cisco CLI emulation).
 
<b>-d</b> <br> Enable debug messages.

<b>-l</b> logfile <br> Save the current session to the file logfile. If logfile exists, the data will be appended at the end.

<b>-c</b> conffile <br> Read/write configuration variables from/to conffile.
 
<b>-M</b> <br> Disable MAC spoofing.


# Protocols

The following protocols are implemented in yersinia current version:

- Spanning Tree Protocol (STP and RSTP) <br> 
- Cisco Discovery Protocol (CDP) <br> 
- Hot Standby Router Protocol (HSRP) <br> 
- Dynamic Host Configuration Protocol (DHCP) <br> 
- Dynamic Trunking Protocol (DTP) <br> 
- IEEE 802.1Q <br> 
- VLAN Trunking Protocol (VTP) <br> 
- Inter-Switch Link Protocol (ISL) <br> 


# Protocols Options
<b><h2><u>Spanning Tree Protocol (STP)</b></h2></u>
<b> Spanning Tree Protocol (STP): </b> is a link management protocol that provides path redundancy while preventing undesirable loops in the network. The supported options are:

<b> -version </b> <i> version </i>
<br> BPDU version (0 STP, 2 RSTP, 3 MSTP)

<b>-type</b> <i> type </i>
<br> BPDU type (Configuration, TCN)

<b> -flags </b> <i> flags </i>
<br> BPDU Flags

<b> -id </b> <i> id </i>
<br> BPDU ID

<b> -cost </b> <i> pathcost </i>
<br>BPDU root path cost

<b> -rootid </b> <i> id </i>
<br> BPDU Root ID

<b> -bridgeid </b> <i> id </i>
<br>BPDU Bridge ID

<b> -portid </b> <i> id </i>
<br> BPDU Port ID

<b> -message </b> <i> secs </i>
<br> BPDU Message Age

<b> -max-age </b> <i> secs </i>
<br> BPDU Max Age (default is 20)

<b> -hello </b> <i> secs </i>
<br> BPDU Hello Time (default is 2)

<b> -forward </b> <i> secs </i>
<br> BPDU Forward Delay

<b> -source </b> <i> hw_addr </i>
<br> Source MAC address

<b> -dest </b> <i> hw_addr </i>
<br> Destination MAC address

<b> -interface </b> <i> iface </i>
<br> Set network interface to use

<b> -attack </b> <i> attack </i>
<br> Attack to launch

<b><h2><u>Cisco Discovery Protocol (CDP)</b></h2></u>
<b>Cisco Discovery Protocol (CDP):</b> is a Cisco propietary Protocol which main aim is to let Cisco devices to communicate to each other about their device settings and protocol configurations.
The supported options are:

<b> -source </b> <i> hw_addr </i>
<br> MAC Source Address

<b> -dest </b> <i> hw_addr </i>
<br> MAC Destination Address

<b> -v </b> <i> version </i>
<br> CDP Version

<b> -ttl </b> <i> ttl </i>
<br> Time To Live

<b> -devid </b> <i> id </i>
<br> Device ID

<b> -address </b> <i> address </i>
<br> Device Address

<b> -port </b> <i> id </i>
<br> Device Port

<b> -capability </b> <i> cap </i>
<br> Device Capabilities

<b> -version </b> <i> version </i>
<br> Device IOS Version

<b> -duplex </b> <i> 0|1 </i>
<br> Device Duplex Configuration

<b> -platform </b> <i> platform </i>
<br> Device Platform

<b> -ipprefix </b> <i> ip </i>
<br> Device IP Prefix 

<b> -phello </b> <i> hello </i>
<br> Device Protocol Hello

<b> -mtu </b> <i> mtu </i>
<br> Device MTU

<b> -vtp_mgm_dom </b> <i> domain </i>
<br> Device VTP Management Domain

<b> -native_vlan </b> <i> vlan </i>
<br> Device Native VLAN

<b> -voip_vlan_r </b> <i> req </i>
<br> Device VoIP VLAN Reply

<b> -voip_vlan_q </b> <i> query </i>
<br> Device VoIP VLAN Query

<b> -t_bitmap </b> <i> bitmap </i>
<br> Device Trust Bitmap

<b> -untrust_cos </b> <i> cos </i>
<br> Device Untrusted CoS

<b> -system_name </b> <i> name </i>
<br> Device System Name

<b> -system_oid </b> <i> oid </i>
<br> Device System ObjectID

<b> -mgm_address </b> <i> address </i>
<br> Device Management Address

<b> -location </b> <i> location </i>
<br> Device Location

<b> -attack </b> <i> attack </i>
<br> Attack to launch

<b>Hot Standby Router Protocol (HSRP):</b> <br>
<b> Inter-Switch Link Protocol (ISL): </b> <br>
<b> VLAN Trunking Protocol (VTP): </b> <br>
<b> Dynamic Host Configuration Protocol (DHCP): </b> <br>
<b> IEEE 802.1Q: </b> <br>
<b> Dynamic Trunking Protocol (DTP): </b> <br>

# Attacks
<b><h3>Attacks Implemented in STP:</h3></b>

0: NONDOS attack sending conf BPDU <br>
1: NONDOS attack sending tcn BPDU <br>
2: DOS attack sending conf BPDUs <br>
3: DOS attack sending tcn BPDUs <br>
4: NONDOS attack Claiming Root Role <br>
5: NONDOS attack Claiming Other Role <br>
6: DOS attack Claiming Root Role with MiTM

<b><h3>Attacks Implemented in CDP:</h3></b>
0: NONDOS attack sending CDP packet <br>
1: DOS attack flooding CDP table <br>
2: NONDOS attack Setting up a virtual device

<b><h3>Attacks Implemented in HSRP:</h3></b>
0: NONDOS attack sending raw HSRP packet <br>
1: NONDOS attack becoming ACTIVE router <br>
2: NONDOS attack becoming ACTIVE router (MITM)

<b><h3>Attacks Implemented in DHCP:</h3></b>
0: NONDOS attack sending RAW packet <br>
1: DOS attack sending DISCOVER packet <br>
2: NONDOS attack creating DHCP rogue server <br>
3: DOS attack sending RELEASE packet

<b><h3>Attacks Implemented in DTP:</h3></b>
0: NONDOS attack sending DTP packet <br>
1: NONDOS attack enabling trunking

<b><h3>Attacks Implemented in 802.1Q:</h3></b>
0: NONDOS attack sending 802.1Q packet <br>
1: NONDOS attack sending 802.1Q double enc. packet <br>
2: DOS attack sending 802.1Q arp poisoning

<b><h3>Attacks Implemented in VTP:</h3></b>
0: NONDOS attack sending VTP packet <br>
1: DOS attack deleting all VTP vlans <br>
2: DOS attack deleting one vlan <br>
3: NONDOS attack adding one vlan <br>
4: DOS attack Catalyst zero day

<b><h3>Attacks Implemented in ISL:</h3></b>
None at the moment <br>

# Gtk GUI
The GTK GUI (<b>-G</b>) is a GTK graphical interface with all of the yersinia powerful features and a professional 'look and feel'.

# Ncurses GUI
The ncurses GUI (<b>-I</b>) is a ncurses (or curses) based console where the user can take advantage of yersinia powerful features.
Press 'h' to display the Help Screen and enjoy your session :)

# Network Daemon
The Network Daemon (<b>-D</b>) is a telnet based server (ala Cisco mode) that listens by default in port 12000/tcp waiting for incoming telnet connections.
It supports a CLI similar to a Cisco device where the user (once authenticated) can display different settings and can launch attacks without having yersinia running in her own machine (specially useful for Windows users).

# Examples

- Send a Rapid Spanning-Tree BPDU with port role designated, port state agreement, learning and port id 0x3000 to eth1:
```
yersinia stp -attack 0 -version 2 -flags 5c -portid 3000 -interface eth1
```

- Start a Spanning-Tree nonDoS root claiming attack in the first nonloopback interface (keep in mind that this kind of attack will use the first BPDU on the network interface to fill in the BPDU fields properly):
```
yersinia stp -attack 4
```

- Start a Spanning-Tree DoS attack sending TCN BPDUs in the eth0 interface with MAC address 66:66:66:66:66:66:

```
yersinia stp -attack 3 -source 66:66:66:66:66:66
```



# fix CDP (mikrotik)

```
/ip neighbor discovery-settings set discover-interface-list=none protocol=lldp,mndp
```

