# SSHFS (protocol sftp) RECOMMEND

<h2>Install package</h2>

```

sudo apt -y update 
sudo apt -y install openssh-server
sudo apt -y install sshfs

```

<h2>Mount</h2>

```

sshfs <username>@ip_host:/<folder_share>/ /<destination_folder>/

```

# SAMBA (protocol smb)

Install package (to server)
```
sudo apt update
sudo apt -y install samba cifs-utils

```

Install package (to client)
```
sudo apt update
sudo apt -y install smbclient

```


<h2>Mount</h2>

```

mount -t cifs //192.168.0.1/backup /mnt/backup -o username=<name>,password=<password>

```

Connect with other port
```

mount -t cifs //192.168.0.1/backup /mnt/backup -o username=<name>,password=<password>,port=<port>

```

<h2>Mount other folder</h2>

```

mount --bind /mnt/backup/ /home/arifzxc/Documents/DataServer/

```




