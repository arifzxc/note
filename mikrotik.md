Block ping
```
ip firewall filter add chain=input protocol=icmp in-interface=<interface> action=drop
```
Block connection port when user use portscanner (nmap)
```
ip firewall filter add chain=input protocol=tcp  in-interface=<interface> action=tarpit 
``` 