# Webmin

<b><u><h2>Webmin</b></u> is software that makes it easy for us to configure Linux servers using graphical displays.</h2>

use ssleay if you want to access it with <b>HTTPS</b>, if you can't skip it:
```
sudo apt -y install libnet-ssleay-perl
```

<h3>Download software</h3>

```
http://www.webmin.com/download.html
```

or

```
cd /usr/local/src
wget https://onboardcloud.dl.sourceforge.net/project/webadmin/webmin/2.010/webmin-2.010.tar.gz
```

<h3>Extrack and setup Webmin configure</h3>

```
tar zxvf webmin-2.010.tar.gz
/usr/local/src/webmin-2.010/./setup.sh
```

follow answer:
```
1. /etc/webmin
2. /var/webmin
3. /usr/bin/perl
4. 10000                #port
5. admin                #user login
6. password             #password login
7. password             #password login
8. y
9. y
```

<h3>Access webmin</h3>

example:
- https://localhost:port
- https://ip_server:port
- https://localhost:10000
- https://192.168.0.177:10000

example, if not use ssleay for HTTPS access
- http://localhost:port
- http://ip_server:port
- http://localhost:10000
- http://192.168.0.177:10000

```
user:       admin
password:   password
```