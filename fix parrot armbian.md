access root user
```
sudo su
```

fix vi (jika error)
```
vi .vimrc
```

change hostname
```
vi /etc/hosts
```
edit
```
127.0.1.1 localhost
127.0.1.1 <new_hostname>
```

```
vi /etc/hostname
```
```
<new_hostname>
```

change timezone
```
timedatectl set-timezone Asia/Jakarta 
```

set locale
locale setting indonesia
```
locale-gen id_ID.UTF-8
```

search = #id_ID.UTF-8 (delete #)
```
sudo vi /etc/locale.gen
```
```
sudo locale-gen
```

update locale
```
update-locale LANG=en_US.UTF-8
update-locale LC_ALL=id_ID.UTF-8
update-locale LANGUAGE=en_US.UTF-8
update-locale LC_MESSAGES=en_US.UTF-8
```