
<h2>Step 1 (Optional)</h2>

Select text editor for you use
```
sudo update-alternatives --config editor
```

<h2>Step 2</h2>

```
sudo vi ~/.selected_editor
```
change nano to vim
```
SELECTED_EDITOR="/usr/bin/vim.basic" 
```