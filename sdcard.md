<h2>Format SD Card</h2>

format fat32
```
mkfs.vfat /dev/sda
```
format ntfs
```
mkfs.ntfs /dev/sda
```
format exfat
```
mkfs.exfat /dev/sda
```

<b>Alternatif</b>

```
sudo dd if=/dev/zero of=/dev/sdb bs=4096 status=progress
```
```
sudo wipefs --all /dev/sda
```


<h2>Burning SD Card</h2>


extension .img
```
dd bs=1M if=Armbian_20.10_Arm-64_bullseye_current_5.9.0.img of=/dev/sdb
sync
```

extension .img.xz
```
unxz --stdout Armbian_20.10_Arm-64_bullseye_current_5.9.0.img.xz | dd of=/dev/sdb
sync
```

<h2>Extract</h2>

format img.gz
```
gunzip Armbian_20.10_Arm-64_bullseye_current_5.9.0.img.gz
```

