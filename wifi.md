# Create Hotspot

```
sudo su
nmcli con add type wifi ifname wlan1 con-name "seamless@wifi.id" autoconnect yes ssid "seamless@wifi.id"
nmcli con modify "seamless@wifi.id" 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared
nmcli con modify "seamless@wifi.id" wifi-sec.key-mgmt wpa-psk
nmcli con modify "seamless@wifi.id" wifi-sec.psk "internet"
nmcli con up "seamless@wifi.id"
nmcli connection show "seamless@wifi.id"
```

# Connect wifi

```
sudo su
nmcli d wifi list ifname wlan0 #untuk cek ssid disekitar
nmcli d wifi connect "nama wifi" password "inputpassword" ifname wlan0  #perintah untuk konek ke wifi
nmcli d disconnect wlan0 #untuk diskonekan wifi
```