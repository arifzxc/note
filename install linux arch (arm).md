Download Armbian Bullseye

- https://androidfilehost.com/?fid=10763459528675575685

Download Arch ArchLinuxARM
- http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz

install prerequisites
```
sudo apt install xz-utils -y
```
extract downloaded armbian image
```
unxz ~/Downloads/Armbian_20.10_Arm-64_bullseye_current_5.9.0.img.xz
```
list out disks
```
sudo fdisk -l
```
run the following to restore the Armbian .img to the target device
replace sdx with the correct source disk
MAKE ABSOLUTELY CERTAIN THE OUTPUT TARGET IS CORRECT
OR YOU COULD POTENTIALLY DAMAGE YOUR OS
OR OTHER STORAGE DEVICES ATTACHED TO THE PC
restore img
```
sudo dd if=~/Downloads/Armbian_20.10_Arm-64_bullseye_current_5.9.0.img of=/dev/sda bs=4M status=progress
```
after the dd completes, run the following to list disks
```
fdsik -l
```
mount partition boot
```
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot
cd /mnt/boot
```
edit extlinux/extlinux.conf
```
vi /mnt/boot/extlinux/extlinux.conf
```
input
```
LABEL Armbian
LINUX /zImage
INITRD /uInitrd

# aml s9xxx
FDT /dtb/amlogic/meson-gxl-s905x-p212.dtb
APPEND root=LABEL=ROOTFS rootflags=data=writeback rw console=ttyAML0,115200n8 console=tty0 no_console_suspend consoleblank=0 fsck.fix=yes fsck.repair=yes net.ifnames=0
```
change name u-boot
```
cd /mnt/boot
mv u-boot-s905x-s912 u-boot.ext
```

mount partition root
```
mkdir -p /mnt/root
mount /dev/sda2 /mnt/root
```

locate the ROOT partition on the SD card, using sda2 from above
delete the armbian OS filesystem, replace the path with the SD card ROOT partition
```
sudo rm /mnt/root/* -r
```
extract the archlinux root to the SD card ROOT partition, replace the path with the SD card ROOT partition
```
sudo tar -xf ArchLinuxARM-aarch64-latest.tar.gz -C /mnt/root
```
add permitroot (optional)
```
vi /mnt/root/etc/ssh/sshd_config
```
```
PermitRootLogin yes
```
after done all, unmount disk
```
umount /mnt/root /mnt/boot/
```


# After successfull install

change the root password
```
passwd
```
delete the default alarm user
```
userdel -r alarm
```
initialize pacman keyring
```
pacman-key --init
pacman-key --populate archlinuxarm
```
update software repositories
```
pacman -Syu
```
install sudo
```
pacman -S sudo
```
create a new user account
change arifzxc in all the commands below to the new username
```
useradd --create-home arifzxc
```
set the new user's password
```
passwd arifzxc
```
add the user to the wheel group
```
usermod -aG wheel arifzxc
```
add the user to the sudoers file
```
vi /etc/sudoers
```
uncomment the following line by removing the # symbol
```
%wheel ALL=(ALL) ALL
```

exit root user, and login normal user
```
exit
```
login normal user, after login testing with sudo 
```
sudo pacman -Syu
```

